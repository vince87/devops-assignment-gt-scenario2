## create dockerfile & push dockerhub

```

 docker build -t myincremental_decremental-web:v2 .

 docker run -d -p 80:80 myincremental_decremental-web:v2 # verify webpage return correctly

 docker tag myincremental_decremental-web:v2 vince87/myincremental_decremental-web:v2

 docker login

 docker push vince87/myincremental_decremental-web:v2

 ```

### Assumption
```
- i will be pushing to any docker registry of my preference
- it will be using same index.html which provided on scenario1
- i will be print my first_name using header of my preference
- i will be using docker environment variable to print my first_name using dockerfile
- i can access via mysql-client since no code changes is required to conenct to mysql
- i can monitor the resource using default provided by prometheus & grafana since the metrics provided
```

### troubleshooting step during the deployment

```
the image which i pushed to docker hub using macbook, always return "Back-off restarting failed container",
after trying several image version using ubuntu or nginx, i drilled down its due to different OS/ARCH on my 
macbook hence i successfully deploy the using ubuntu os.

i need to verify i can connect to mysql hence i need to change my image to ubuntu as the nginx images not able to install mysql-client to verify i can connect to mysql

vincent@Vincents-MacBook-Air docker % kubectl get po

NAME    READY   STATUS   RESTARTS     AGE

nginx   0/1     Error    1 (2s ago)   4s

```

## deploy kubernetes manifest file

```
kubectl apply -f manifest .
storageclass.storage.k8s.io/managed-premium-retain-sc unchanged
persistentvolumeclaim/azure-managed-disk-pvc unchanged
configmap/usermanagement-dbcreation-script unchanged
deployment.apps/mysql unchanged
service/mysql unchanged
deployment.apps/incremental-decremental-web configured
service/incremental-decremental-web-service unchanged
secret/mysql-db-password unchanged
```

## Verify pod/service is sucessfully deploy

```

kubectl get po
NAME                                          READY   STATUS    RESTARTS   AGE
incremental-decremental-web-5d4986c5d-rkwlv   1/1     Running   0          14m
mysql-5bbdc744c9-lhccs                        1/1     Running   0          167m

kubectl describe po incremental-decremental-web-5d4986c5d-rkwlv

kubectl describe po mysql-5bbdc744c9-lhccs

kubectl get svc

```

## Ensure incremental and decremental counter can connect to mysql

```

kubectl exec -it incremental-decremental-web-5d4986c5d-rkwlv -- /bin/sh

mysql -h mysql -p<mysql-password>

mysql> show schemas;
+---------------------+
| Database            |
+---------------------+
| information_schema  |
| incrementaldb       |
| #mysql50#lost+found |
| mysql               |
| performance_schema  |
+---------------------+
5 rows in set (0.00 sec)

mysql> use incrementaldb;
Database changed

```

### create table on mysql
```

mysql> CREATE TABLE Persons (
    -> PersonID int,
    -> LastName varchar(255),
    -> FirstName varchar(255),
    -> Address varchar(255),
    -> City varchar(255)
    -> );
Query OK, 0 rows affected (0.06 sec)

mysql> show tables;
+-------------------------+
| Tables_in_incrementaldb |
+-------------------------+
| Persons                 |
+-------------------------+
1 row in set (0.00 sec)

```

## setup Prometheus & grafana

```

helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo add stable https://charts.helm.sh/stable
helm repo update

helm install prometheus prometheus-community/kube-prometheus-stack

NAME: prometheus
LAST DEPLOYED: Mon Mar  6 07:32:14 2023
NAMESPACE: default
STATUS: deployed
REVISION: 1
NOTES:
kube-prometheus-stack has been installed. Check its status by running:
  kubectl --namespace default get pods -l "release=prometheus"

Visit https://github.com/prometheus-operator/kube-prometheus for instructions on how to create & configure Alertmanager and Prometheus instances using the Operator.

```

### expose svc to nodeport to access externally for prometheus & grafana
```
kubectl apply -f manifest/prometheus-ext.yml
kubectl apply -f manifest/grafana-ext.yml
```
### retrieve default grafana login credential
```
kubectl get secret prometheus-grafana -o yaml
```
### monitor data source for prometheus 

<img src="Prometheus_PromQL.png" alt="Alt text" title="Prometheus">

### visualise dashboard using grafana

<img src="grafana.png" alt="Alt text" title="grafana">