# Base image
FROM ubuntu

RUN apt-get -y update && apt-get -y install nginx

RUN apt-get install mysql-client -y

COPY default /etc/nginx/sites-available/default

ENV FIRST_NAME="Vincent"

COPY index.html /usr/share/nginx/html/

RUN echo "<h1>Hello, $FIRST_NAME!</h1>" >> /usr/share/nginx/html/index.html

EXPOSE 80

CMD ["/usr/sbin/nginx", "-g", "daemon off;"]